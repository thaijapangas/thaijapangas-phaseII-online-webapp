﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entitys.Items
{
    public class entItems
    {
        public string guid { get; set; }
        public string temp_item_code { get; set; }
        public string full_item_code { get; set; }
        public string item_name { get; set; }
        public string is_active { get; set; }
        public string create_by { get; set; }
        public DateTime create_date { get; set; }
        public string update_by { get; set; }
        public DateTime update_date { get; set; }
    }
}