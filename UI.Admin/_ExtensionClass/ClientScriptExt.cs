﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.IO;
using System.Text;


public enum JMessageType
{
    Warning,
    Accept,
    Error
}

public static class ClientScriptExt
{
    public static void ScriptPageRegister(this Page thisPage, string script)
    {
        //Register JavaScript In Ajax PostBack
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(thisPage, thisPage.GetType(), Guid.NewGuid().ToString().Replace("-", "_"), script, true);
    }
    public static void ScriptPageRegister(this Page thisPage, string script, string _keyName)
    {
        //Register JavaScript In Ajax PostBack
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(thisPage, thisPage.GetType(), _keyName, script, true);
    }
    public static void ScriptAlert(this Page thisPage, string message)
    {
        //Register JavaScript In Ajax PostBack
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(thisPage, thisPage.GetType(), "Alert", @"alert('" + message + "');", true);
    }

    public static void ScriptException(this Page thisPage, Exception exception)
    {
        var pageName = Path.GetFileName(thisPage.Request.Path);
        var pageUrl = Path.GetFileName(thisPage.Request.Url.PathAndQuery);

        var msg = string.Empty;
        if (exception.InnerException != null)
        {
            msg = exception.InnerException.Message;
        }
        else if (exception.Message != string.Empty)
        {
            msg = exception.Message;
        }
        else
        {
            msg = exception.ToString();
        }

        var exceptionMsg = "Exception Detail : " + msg + "<br/><br/>Exception Page : " + pageName;
        ScriptJqueryMessage(thisPage, exceptionMsg, JMessageType.Warning);

    }


    public static void ToAppendTextScript(this Exception _ex, StringBuilder _sb)
    {
        if (_ex != null)
        {
            _sb.Append("<div style=\"text-align: left;\">Source : " + _ex.Source);
            _sb.Append("<br/>Inner Exception Text : " + _ex.Message.Replace("\r", " ").Replace("\n", "<br/>").Replace("'", "") + "</div><br/>");
            if (_ex.InnerException != null)
            {
                _ex.InnerException.ToAppendTextScript(_sb);
            }
        }
    }
    public static void ToAppendTextScript(this Exception _ex, string _str)
    {
        if (_ex != null)
        {
            _str += "<div style=\"text-align: left;\">Inner Exception Text : <br/>"
              + _ex.Message.Replace("\r", " ").Replace("\n", "<br/>").Replace("'", "") + "</div><br/>";
            _str += "<div style=\"text-align: center;\">Source : <br/>"
                    + _ex.Source.Replace("'", "") + "</div><br/>";
            if (_ex.InnerException != null)
            {
                _ex.InnerException.ToAppendTextScript(_str);
            }
        }
    }
    public static void ScriptJqueryMessage(this Page thisPage, string message, JMessageType msgType)
    {

        string contentStyle = string.Empty;
        string headTitle = string.Empty;
        bool isAutoHide = false;

        switch (msgType)
        {
            case JMessageType.Accept:
                contentStyle = "content_accept";
                headTitle = "Success";
                isAutoHide = true;
                break;
            case JMessageType.Warning:
                contentStyle = "content_warning";
                headTitle = "Warning";
                isAutoHide = true; // Auto hide message
                break;
            case JMessageType.Error:
                contentStyle = "content_warning";
                headTitle = "Warning";
                break;
            default:
                break;
        }

        string AppendElement = "<div id=\"frmMsg\" class=\"" + contentStyle + "\">"
           + "<div style=\"width:100%;line-height:25px;\">"
           + "<div style=\"text-align: left;float:left;padding-left:10px;font-size:13px;color:#fff;\">" + headTitle + "</div>"
           + "<div style=\"text-align: right;float:right;\"><button id=\"btClose\" type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>"
           + "<div style=\"clear:both;\"></div>"
           + "</div>"
           + "<div id=\"divMsg\" class=\"content_text-msg \">" + message + ""
           + "</div></div>";

        AppendElement = AppendElement.Replace("'", "");
        System.Web.UI.ScriptManager.RegisterStartupScript(thisPage, thisPage.GetType(),
            "JqueryMessage", @"$(document).ready(function() {" +
               @"$('#frmMsg').remove();" +
               @"$('body').append('" + AppendElement + "');" +
               @"$('#btClose').bind('click', function() { $('#frmMsg').fadeOut(300); } );" +
               @"$('#frmMsg').fadeIn(300)" + ((isAutoHide == true) ? ".delay(1500).fadeOut(300)" : "") + ";});", true);
    }
}