﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="UI.Admin.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Thai Japan Gas</title>
     <script src="_js/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="_css/btnbootstrap.css" />
    <link rel="stylesheet" href="_css/textbox.css" />
    <link rel="stylesheet" href="_css/element-view.css" />
    <script type="text/javascript">
        //Clear text control.
        function clear_text() {
            $('#txtlogin_name').val('');
            $('#txtlogin_name').focus();
            return false;
        }
    </script>
</head>
<body style="font-family: 'Open Sans', sans-serif;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server" />
        <asp:UpdatePanel ID="upnmain" runat="server">
            <ContentTemplate>
                <div id="divHead">
                    <table style="width: 95%; margin-left: auto; margin-right: auto;" cellpadding="3" cellspacing="3">
                        <tr>
                            <td style="height: 50px;"></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-weight: bold;">
                                <table style="width: 40%; background-color: #f2f1f1; border-radius: 8px 8px 8px 8px;" cellpadding="2"
                                    cellspacing="2">
                                    <tr>
                                        <td style="background-color: #006699; width: 100%; height: 45px; border-radius: 6px 6px 6px 6px; text-align: center;">
                                            <asp:Label ID="lblTitle" runat="server" ForeColor="White" Font-Size="30px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 15px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblVersion" runat="server" Text="Version : 1.0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblProjectTitle" runat="server" Text="TJG Project"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblDevelopBy" runat="server" Text="developped by SysXoft Co.,Ltd"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom: 1px dashed #ccc; padding-top: 20px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px"></td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <div style="width: 550px;">
                                                <asp:Image ID="imgLogo" runat="server" ToolTip="Logo" ImageUrl="~/_images/img_login.png" ImageAlign="Left" Width="100px" Height="100px" />
                                                <br />
                                                <asp:TextBox ID="txtlogin_name" runat="server" CssClass="txt_login" TextMode="Password" placeholder="Password"></asp:TextBox><br />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="btnSubmit_Click"  />
                                            &nbsp;&nbsp;
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-danger" OnClick="btnCancel_Click"  />&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px;"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
