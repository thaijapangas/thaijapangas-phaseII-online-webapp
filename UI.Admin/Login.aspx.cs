﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        #region Focus TextBox

        void PageSetFocus(Control ctrl)
        {
            ScriptManager.GetCurrent(this.Page).SetFocus(ctrl);
        }

        #endregion

        #region Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["secLogin"] = null;
                PageSetFocus(txtlogin_name);
            }

            this.lblTitle.Text = string.IsNullOrEmpty(ConfigurationManager.AppSettings["TitleName"].ToString()) ? "Thai Japan Gas" : ConfigurationManager.AppSettings["TitleName"].ToString().Trim();
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtlogin_name.Text.Trim()))
            {
                // Password empty.
                Page.ScriptJqueryMessage("Please enter password", JMessageType.Warning);
                PageSetFocus(this.txtlogin_name);
            }
            else
            {
                string _pass = string.IsNullOrEmpty(ConfigurationManager.AppSettings["Password"].ToString()) ? "Thai Japan Gas" : ConfigurationManager.AppSettings["Password"].ToString().Trim();
                if (this.txtlogin_name.Text.Trim() == _pass.Trim())
                {
                    //True.
                    Session["secLogin"] = "OK";
                    Response.Redirect("Mamgements.aspx");
                }
                else
                {
                    // Password in correct.
                    Page.ScriptJqueryMessage("This password not match", JMessageType.Warning);
                    PageSetFocus(this.txtlogin_name);
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.txtlogin_name.Text = string.Empty;
            PageSetFocus(txtlogin_name);
        }

        #endregion
    }
}