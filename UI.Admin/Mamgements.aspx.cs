﻿using DataAccess.Customers;
using Entitys.Customers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class Mamgements : System.Web.UI.Page
    {
        #region Member

        // Page Customer
        int Cus_pageSize = 15;
        //int totalUsers = 0;
        int Cus_totalPages = 0;
        int Cus_currentPage = 1;

        // Page Items
        int Items_pageSize = 15;
        int Items_totalPages = 0;
        int Items_currentPage = 1;

        //DataTable
        DataTable dtReload;
        DataTable dtReloadItem;
        DataTable dtRead = new DataTable();

        #endregion

        #region Focus TextBox

        void PageSetFocus(Control ctrl)
        {
            ScriptManager.GetCurrent(this.Page).SetFocus(ctrl);
        }

        #endregion

        #region Method

        // Paging Items.
        private void ReloadItems()
        {
            dtReloadItem = DataAccess.Items.Items.Instance.getItems();
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("temp_item_code");
            dtSubReload.Columns.Add("full_item_code");
            dtSubReload.Columns.Add("item_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            if (dtReloadItem.Rows.Count > 0)
            {
                dgvItems.DataSource = dtReloadItem;
                dgvItems.DataBind();

                //Calculate split page.
                int counter = 0;
                int pageIndex = Items_currentPage - 1;
                int startIndex = Items_pageSize * pageIndex;
                int endIndex = startIndex + Items_pageSize - 1;

                if (dtReloadItem.Rows.Count > 0 || dtReloadItem != null)
                {
                    foreach (DataRow itemReload in dtReloadItem.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["guid"].ToString().Trim();
                            drReload[1] = itemReload["temp_item_code"].ToString().Trim();
                            drReload[2] = itemReload["full_item_code"].ToString().Trim();
                            drReload[3] = itemReload["item_name"].ToString().Trim();
                            drReload[4] = itemReload["is_active"].ToString().Trim();
                            drReload[5] = itemReload["create_date"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblItemsSearchingCount.Visible = false;
                }
                else if (dtReloadItem.Rows.Count == 0 || dtReloadItem == null)
                {
                    //Not found data.
                    this.lblItemsSearchingCount.Visible = true;
                    this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvItems.DataSource = dtReloadItem;
                    dgvItems.DataBind();

                    ItemsNextButton.Visible = false;
                    ItemsEndpage.Visible = false;
                    ItemsPreviousButton.Visible = false;
                    ItemsFirstpage.Visible = false;

                    this.lblItemspageIndex.Visible = false;
                    this.txtItemspageIndex.Visible = false;
                    this.btnItemsGO.Visible = false;

                    ItemsCurrentPageLabel.Text = "0";
                    ItemsTotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                ItemsNextButton.Visible = true;
                ItemsEndpage.Visible = true;
                ItemsPreviousButton.Visible = true;
                ItemsFirstpage.Visible = true;

                dgvItems.DataSource = dtSubReload;

                Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

                if (Items_currentPage > Items_totalPages)
                {
                    Items_currentPage = Items_totalPages;
                    ReloadItems();
                    return;
                }

                dgvItems.DataBind();
                ItemsCurrentPageLabel.Text = Items_currentPage.ToString();
                ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

                if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblItemspageIndex.Visible = false;
                    this.txtItemspageIndex.Visible = false;
                    this.btnItemsGO.Visible = false;
                }
                else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblItemspageIndex.Visible = true;
                    this.txtItemspageIndex.Visible = true;
                    this.btnItemsGO.Visible = true;
                }

                if (Items_currentPage == Items_totalPages)
                {
                    ItemsNextButton.Visible = false;
                    ItemsEndpage.Visible = false;
                }
                else
                {
                    ItemsNextButton.Visible = true;
                    ItemsEndpage.Visible = true;
                }

                if (Items_currentPage == 1)
                {
                    ItemsFirstpage.Visible = false;
                    ItemsPreviousButton.Visible = false;
                }
                else
                {
                    ItemsFirstpage.Visible = true;
                    ItemsPreviousButton.Visible = true;
                }

                if (dtReloadItem.Rows.Count <= 0)
                    NavigationPanelItems.Visible = false;
                else
                    NavigationPanelItems.Visible = true;
            }
        }
        void startPagingItems(int startPage)
        {
            this.txtItemspageIndex.Text = string.Empty;
            dtReloadItem = DataAccess.Items.Items.Instance.getItems();

            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("temp_item_code");
            dtSubReload.Columns.Add("full_item_code");
            dtSubReload.Columns.Add("item_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = startPage - 1;
            int startIndex = Items_pageSize * pageIndex;
            int endIndex = startIndex + Items_pageSize - 1;

            if (dtReloadItem.Rows.Count > 0 || dtReloadItem != null)
            {
                foreach (DataRow itemReload in dtReloadItem.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["guid"].ToString().Trim();
                        drReload[1] = itemReload["temp_item_code"].ToString().Trim();
                        drReload[2] = itemReload["full_item_code"].ToString().Trim();
                        drReload[3] = itemReload["item_name"].ToString().Trim();
                        drReload[4] = itemReload["is_active"].ToString().Trim();
                        drReload[5] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblItemsSearchingCount.Visible = false;
            }
            else if (dtReload == null || dtReload.Rows.Count == 0)
            {
                //Not found data.
                this.lblItemsSearchingCount.Visible = true;
                this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvItems.DataSource = dtReloadItem;
                dgvItems.DataBind();

                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
                ItemsPreviousButton.Visible = false;
                ItemsFirstpage.Visible = false;

                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;

                ItemsCurrentPageLabel.Text = "0";
                ItemsTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            ItemsNextButton.Visible = true;
            ItemsEndpage.Visible = true;
            ItemsPreviousButton.Visible = true;
            ItemsFirstpage.Visible = true;

            dgvItems.DataSource = dtSubReload;

            Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

            if (startPage > Items_totalPages)
            {
                startPage = Items_totalPages;
                startPagingItems(startPage);
                return;
            }

            dgvItems.DataBind();
            ItemsCurrentPageLabel.Text = startPage.ToString();
            ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

            if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;
            }
            else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblItemspageIndex.Visible = true;
                this.txtItemspageIndex.Visible = true;
                this.btnItemsGO.Visible = true;
            }

            if (startPage == Items_totalPages)
            {
                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
            }
            else
            {
                ItemsNextButton.Visible = true;
                ItemsEndpage.Visible = true;
            }

            if (startPage == 1)
            {
                ItemsFirstpage.Visible = false;
                ItemsPreviousButton.Visible = false;
            }
            else
            {
                ItemsFirstpage.Visible = true;
                ItemsPreviousButton.Visible = true;
            }

            if (dtReloadItem.Rows.Count <= 0)
                NavigationPanelItems.Visible = false;
            else
                NavigationPanelItems.Visible = true;
        }
        void endPagingItems(int lastPage)
        {
            this.txtItemspageIndex.Text = string.Empty;
            dtReloadItem = DataAccess.Items.Items.Instance.getItems();
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("temp_item_code");
            dtSubReload.Columns.Add("full_item_code");
            dtSubReload.Columns.Add("item_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = lastPage - 1;
            int startIndex = Items_pageSize * pageIndex;
            int endIndex = startIndex + Items_pageSize - 1;

            if (dtReloadItem.Rows.Count > 0 || dtReloadItem != null)
            {
                foreach (DataRow itemReload in dtReload.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["guid"].ToString().Trim();
                        drReload[1] = itemReload["temp_item_code"].ToString().Trim();
                        drReload[2] = itemReload["full_item_code"].ToString().Trim();
                        drReload[3] = itemReload["item_name"].ToString().Trim();
                        drReload[4] = itemReload["is_active"].ToString().Trim();
                        drReload[5] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblItemsSearchingCount.Visible = false;
            }
            else if (dtReloadItem == null || dtReloadItem.Rows.Count == 0)
            {
                //Not found data.
                this.lblItemsSearchingCount.Visible = true;
                this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvItems.DataSource = dtReloadItem;
                dgvItems.DataBind();

                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
                ItemsPreviousButton.Visible = false;
                ItemsFirstpage.Visible = false;

                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;

                ItemsCurrentPageLabel.Text = "0";
                ItemsTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            ItemsNextButton.Visible = true;
            ItemsEndpage.Visible = true;
            ItemsPreviousButton.Visible = true;
            ItemsFirstpage.Visible = true;

            dgvItems.DataSource = dtSubReload;

            Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

            if (lastPage > Items_totalPages)
            {
                lastPage = Items_totalPages;
                endPagingItems(lastPage);
                return;
            }

            dgvItems.DataBind();
            ItemsCurrentPageLabel.Text = lastPage.ToString();
            ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

            if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;
            }
            else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblItemspageIndex.Visible = true;
                this.txtItemspageIndex.Visible = true;
                this.btnCusGO.Visible = true;
            }

            if (lastPage == Items_totalPages)
            {
                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
            }
            else
            {
                ItemsNextButton.Visible = true;
                ItemsEndpage.Visible = true;
            }

            if (lastPage == 1)
            {
                ItemsFirstpage.Visible = false;
                ItemsPreviousButton.Visible = false;
            }
            else
            {
                ItemsFirstpage.Visible = true;
                ItemsPreviousButton.Visible = true;
            }

            if (dtReloadItem.Rows.Count <= 0)
                NavigationPanelItems.Visible = false;
            else
                NavigationPanelItems.Visible = true;
        }
        void gotoPageItems(int index)
        {
            if (index <= 0)
            {
                if (this.ItemsTotalPagesLabel.Text.Trim() == "0")
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                    this.txtItemspageIndex.Text = string.Empty;
                    return;
                }
                else
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                    this.txtItemspageIndex.Text = string.Empty;
                    return;
                }
            }
            else
            {
                if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
                {
                    this.txtItemspageIndex.Attributes.Add("onfocus", "selectText();");
                    PageSetFocus(this.txtItemspageIndex);
                    return;
                }
                else if (index > Convert.ToInt32(this.ItemsTotalPagesLabel.Text.Trim()))
                {
                    this.txtCuspageIndex.Text = string.Empty;
                    PageSetFocus(this.txtItemspageIndex);
                    return;
                }
                else
                {
                    #region Go to Page

                    dtReload = Customer.Instance.getCustomer();
                    DataTable goPage = new DataTable();

                    //Add colunms
                    goPage.Columns.Add("guid");
                    goPage.Columns.Add("temp_item_code");
                    goPage.Columns.Add("full_item_code");
                    goPage.Columns.Add("item_name");
                    goPage.Columns.Add("is_active");
                    goPage.Columns.Add("create_date");

                    int counter = 0;
                    int pageIndex = index - 1;
                    int startIndex = Cus_pageSize * pageIndex;
                    int endIndex = startIndex + Cus_pageSize - 1;

                    if (dtReloadItem.Rows.Count > 0 || dtReloadItem != null)
                    {
                        foreach (DataRow itemReload in dtReloadItem.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drgoPage = goPage.NewRow();
                                drgoPage[0] = itemReload["guid"].ToString().Trim();
                                drgoPage[1] = itemReload["temp_item_code"].ToString().Trim();
                                drgoPage[2] = itemReload["full_item_code"].ToString().Trim();
                                drgoPage[3] = itemReload["item_name"].ToString().Trim();
                                drgoPage[4] = itemReload["is_active"].ToString().Trim();
                                drgoPage[5] = itemReload["create_date"].ToString().Trim();
                                goPage.Rows.Add(drgoPage);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblItemsSearchingCount.Visible = false;
                    }
                    else if (dtReloadItem == null || dtReloadItem.Rows.Count == 0)
                    {
                        //Not found data.
                        this.lblItemsSearchingCount.Visible = true;
                        this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvItems.DataSource = goPage;
                        dgvItems.DataBind();

                        ItemsNextButton.Visible = false;
                        ItemsEndpage.Visible = false;
                        ItemsPreviousButton.Visible = false;
                        ItemsFirstpage.Visible = false;

                        this.lblItemspageIndex.Visible = false;
                        this.txtItemspageIndex.Visible = false;
                        this.btnItemsGO.Visible = false;

                        ItemsCurrentPageLabel.Text = "0";
                        ItemsTotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    ItemsNextButton.Visible = true;
                    ItemsEndpage.Visible = true;
                    ItemsPreviousButton.Visible = true;
                    ItemsFirstpage.Visible = true;

                    dgvItems.DataSource = goPage;

                    Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

                    if (index > Items_totalPages)
                    {
                        index = Items_totalPages;
                        gotoPageItems(index);
                        return;
                    }

                    dgvItems.DataBind();
                    ItemsCurrentPageLabel.Text = index.ToString();
                    ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

                    if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblItemspageIndex.Visible = false;
                        this.txtItemspageIndex.Visible = false;
                        this.btnItemsGO.Visible = false;
                    }
                    else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblItemspageIndex.Visible = true;
                        this.txtItemspageIndex.Visible = true;
                        this.btnItemsGO.Visible = true;
                    }

                    if (index == Items_totalPages)
                    {
                        ItemsNextButton.Visible = false;
                        ItemsEndpage.Visible = false;
                    }
                    else
                    {
                        ItemsNextButton.Visible = true;
                        ItemsEndpage.Visible = true;
                    }

                    if (index == 1)
                    {
                        ItemsFirstpage.Visible = false;
                        ItemsPreviousButton.Visible = false;
                    }
                    else
                    {
                        ItemsFirstpage.Visible = true;
                        ItemsPreviousButton.Visible = true;
                    }

                    if (dtReloadItem.Rows.Count <= 0)
                        NavigationPanelItems.Visible = false;
                    else
                        NavigationPanelItems.Visible = true;

                    #endregion
                }
            }
        }

        //Paging Customer.
        private void Reload()
        {
            dtReload = Customer.Instance.getCustomer();
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("customer_code");
            dtSubReload.Columns.Add("customer_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            if (dtReload.Rows.Count > 0)
            {
                dgvCustomer.DataSource = dtReload;
                dgvCustomer.DataBind();

                //Calculate split page.
                int counter = 0;
                int pageIndex = Cus_currentPage - 1;
                int startIndex = Cus_pageSize * pageIndex;
                int endIndex = startIndex + Cus_pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["guid"].ToString().Trim();
                            drReload[1] = itemReload["customer_code"].ToString().Trim();
                            drReload[2] = itemReload["customer_name"].ToString().Trim();
                            drReload[3] = itemReload["is_active"].ToString().Trim();
                            drReload[4] = itemReload["create_date"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblCusSearchingCount.Visible = false;
                }
                else if (dtReload.Rows.Count == 0 || dtReload == null)
                {
                    //Not found data.
                    this.lblCusSearchingCount.Visible = true;
                    this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvCustomer.DataSource = dtReload;
                    dgvCustomer.DataBind();

                    CusNextButton.Visible = false;
                    CusEndpage.Visible = false;
                    CusPreviousButton.Visible = false;
                    CusFirstpage.Visible = false;

                    this.lblCuspageIndex.Visible = false;
                    this.txtCuspageIndex.Visible = false;
                    this.btnCusGO.Visible = false;

                    CusCurrentPageLabel.Text = "0";
                    CusTotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
                CusPreviousButton.Visible = true;
                CusFirstpage.Visible = true;

                dgvCustomer.DataSource = dtSubReload;

                Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

                if (Cus_currentPage > Cus_totalPages)
                {
                    Cus_currentPage = Cus_totalPages;
                    Reload();
                    return;
                }

                dgvCustomer.DataBind();
                CusCurrentPageLabel.Text = Cus_currentPage.ToString();
                CusTotalPagesLabel.Text = Cus_totalPages.ToString();

                if (this.CusTotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblCuspageIndex.Visible = false;
                    this.txtCuspageIndex.Visible = false;
                    this.btnCusGO.Visible = false;
                }
                else if (this.CusTotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblCuspageIndex.Visible = true;
                    this.txtCuspageIndex.Visible = true;
                    this.btnCusGO.Visible = true;
                }

                if (Cus_currentPage == Cus_totalPages)
                {
                    CusNextButton.Visible = false;
                    CusEndpage.Visible = false;
                }
                else
                {
                    CusNextButton.Visible = true;
                    CusEndpage.Visible = true;
                }

                if (Cus_currentPage == 1)
                {
                    CusFirstpage.Visible = false;
                    CusPreviousButton.Visible = false;
                }
                else
                {
                    CusFirstpage.Visible = true;
                    CusPreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanelCustomer.Visible = false;
                else
                    NavigationPanelCustomer.Visible = true;
            }
        }
        void startPaging(int startPage)
        {
            this.txtCuspageIndex.Text = string.Empty;
            dtReload = Customer.Instance.getCustomer();

            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("customer_code");
            dtSubReload.Columns.Add("customer_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = startPage - 1;
            int startIndex = Cus_pageSize * pageIndex;
            int endIndex = startIndex + Cus_pageSize - 1;

            if (dtReload.Rows.Count > 0 || dtReload != null)
            {
                foreach (DataRow itemReload in dtReload.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["guid"].ToString().Trim();
                        drReload[1] = itemReload["customer_code"].ToString().Trim();
                        drReload[2] = itemReload["customer_name"].ToString().Trim();
                        drReload[3] = itemReload["is_active"].ToString().Trim();
                        drReload[4] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblCusSearchingCount.Visible = false;
            }
            else if (dtReload == null || dtReload.Rows.Count == 0)
            {
                //Not found data.
                this.lblCusSearchingCount.Visible = true;
                this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvCustomer.DataSource = dtReload;
                dgvCustomer.DataBind();

                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
                CusPreviousButton.Visible = false;
                CusFirstpage.Visible = false;

                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;

                CusCurrentPageLabel.Text = "0";
                CusTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            CusNextButton.Visible = true;
            CusEndpage.Visible = true;
            CusPreviousButton.Visible = true;
            CusFirstpage.Visible = true;

            dgvCustomer.DataSource = dtSubReload;

            Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

            if (startPage > Cus_totalPages)
            {
                startPage = Cus_totalPages;
                startPaging(startPage);
                return;
            }

            dgvCustomer.DataBind();
            CusCurrentPageLabel.Text = startPage.ToString();
            CusTotalPagesLabel.Text = Cus_totalPages.ToString();

            if (this.CusTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;
            }
            else if (this.CusTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblCuspageIndex.Visible = true;
                this.txtCuspageIndex.Visible = true;
                this.btnCusGO.Visible = true;
            }

            if (startPage == Cus_totalPages)
            {
                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
            }
            else
            {
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
            }

            if (startPage == 1)
            {
                CusFirstpage.Visible = false;
                CusPreviousButton.Visible = false;
            }
            else
            {
                CusFirstpage.Visible = true;
                CusPreviousButton.Visible = true;
            }

            if (dtReload.Rows.Count <= 0)
                NavigationPanelCustomer.Visible = false;
            else
                NavigationPanelCustomer.Visible = true;
        }
        void endPaging(int lastPage)
        {
            this.txtpageIndex.Text = string.Empty;
            dtReload = Customer.Instance.getCustomer();
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("customer_code");
            dtSubReload.Columns.Add("customer_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = lastPage - 1;
            int startIndex = Cus_pageSize * pageIndex;
            int endIndex = startIndex + Cus_pageSize - 1;

            if (dtReload.Rows.Count > 0 || dtReload != null)
            {
                foreach (DataRow itemReload in dtReload.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["guid"].ToString().Trim();
                        drReload[1] = itemReload["customer_code"].ToString().Trim();
                        drReload[2] = itemReload["customer_name"].ToString().Trim();
                        drReload[3] = itemReload["is_active"].ToString().Trim();
                        drReload[4] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblCusSearchingCount.Visible = false;
            }
            else if (dtReload == null || dtReload.Rows.Count == 0)
            {
                //Not found data.
                this.lblCusSearchingCount.Visible = true;
                this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvCustomer.DataSource = dtReload;
                dgvCustomer.DataBind();

                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
                CusPreviousButton.Visible = false;
                CusFirstpage.Visible = false;

                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;

                CusCurrentPageLabel.Text = "0";
                CusTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            CusNextButton.Visible = true;
            CusEndpage.Visible = true;
            CusPreviousButton.Visible = true;
            CusFirstpage.Visible = true;

            dgvCustomer.DataSource = dtSubReload;

            Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

            if (lastPage > Cus_totalPages)
            {
                lastPage = Cus_totalPages;
                endPaging(lastPage);
                return;
            }

            dgvCustomer.DataBind();
            CusCurrentPageLabel.Text = lastPage.ToString();
            CusTotalPagesLabel.Text = Cus_totalPages.ToString();

            if (this.CusTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;
            }
            else if (this.CusTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblCuspageIndex.Visible = true;
                this.txtCuspageIndex.Visible = true;
                this.btnCusGO.Visible = true;
            }

            if (lastPage == Cus_totalPages)
            {
                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
            }
            else
            {
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
            }

            if (lastPage == 1)
            {
                CusFirstpage.Visible = false;
                CusPreviousButton.Visible = false;
            }
            else
            {
                Firstpage.Visible = true;
                PreviousButton.Visible = true;
            }

            if (dtReload.Rows.Count <= 0)
                NavigationPanelCustomer.Visible = false;
            else
                NavigationPanelCustomer.Visible = true;
        }
        void gotoPage(int index)
        {
            if (index <= 0)
            {
                if (this.CusTotalPagesLabel.Text.Trim() == "0")
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                    this.txtCuspageIndex.Text = string.Empty;
                    return;
                }
                else
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                    this.txtCuspageIndex.Text = string.Empty;
                    return;
                }
            }
            else
            {
                if (this.CusTotalPagesLabel.Text.Trim() == "1")
                {
                    this.txtCuspageIndex.Attributes.Add("onfocus", "selectText();");
                    PageSetFocus(this.txtCuspageIndex);
                    return;
                }
                else if (index > Convert.ToInt32(this.CusTotalPagesLabel.Text.Trim()))
                {
                    this.txtCuspageIndex.Text = string.Empty;
                    PageSetFocus(this.txtCuspageIndex);
                    return;
                }
                else
                {
                    #region Go to Page

                    dtReload = Customer.Instance.getCustomer();
                    DataTable goPage = new DataTable();

                    //Add colunms
                    goPage.Columns.Add("guid");
                    goPage.Columns.Add("customer_code");
                    goPage.Columns.Add("customer_name");
                    goPage.Columns.Add("is_active");
                    goPage.Columns.Add("create_date");

                    int counter = 0;
                    int pageIndex = index - 1;
                    int startIndex = Cus_pageSize * pageIndex;
                    int endIndex = startIndex + Cus_pageSize - 1;

                    if (dtReload.Rows.Count > 0 || dtReload != null)
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drgoPage = goPage.NewRow();
                                drgoPage[0] = itemReload["guid"].ToString().Trim();
                                drgoPage[1] = itemReload["customer_code"].ToString().Trim();
                                drgoPage[2] = itemReload["customer_name"].ToString().Trim();
                                drgoPage[3] = itemReload["is_active"].ToString().Trim();
                                drgoPage[4] = itemReload["create_date"].ToString().Trim();
                                goPage.Rows.Add(drgoPage);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblCusSearchingCount.Visible = false;
                    }
                    else if (dtReload == null || dtReload.Rows.Count == 0)
                    {
                        //Not found data.
                        this.lblCusSearchingCount.Visible = true;
                        this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvCustomer.DataSource = goPage;
                        dgvCustomer.DataBind();

                        CusNextButton.Visible = false;
                        CusEndpage.Visible = false;
                        CusPreviousButton.Visible = false;
                        CusFirstpage.Visible = false;

                        this.lblCuspageIndex.Visible = false;
                        this.txtCuspageIndex.Visible = false;
                        this.btnCusGO.Visible = false;

                        CusCurrentPageLabel.Text = "0";
                        CusTotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    CusNextButton.Visible = true;
                    CusEndpage.Visible = true;
                    CusPreviousButton.Visible = true;
                    CusFirstpage.Visible = true;

                    dgvCustomer.DataSource = goPage;

                    Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

                    if (index > Cus_totalPages)
                    {
                        index = Cus_totalPages;
                        gotoPage(index);
                        return;
                    }

                    dgvCustomer.DataBind();
                    CusCurrentPageLabel.Text = index.ToString();
                    CusTotalPagesLabel.Text = Cus_totalPages.ToString();

                    if (this.CusTotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblCuspageIndex.Visible = false;
                        this.txtCuspageIndex.Visible = false;
                        this.btnCusGO.Visible = false;
                    }
                    else if (this.CusTotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblCuspageIndex.Visible = true;
                        this.txtCuspageIndex.Visible = true;
                        this.btnCusGO.Visible = true;
                    }

                    if (index == Cus_totalPages)
                    {
                        CusNextButton.Visible = false;
                        CusEndpage.Visible = false;
                    }
                    else
                    {
                        CusNextButton.Visible = true;
                        CusEndpage.Visible = true;
                    }

                    if (index == 1)
                    {
                        CusFirstpage.Visible = false;
                        CusPreviousButton.Visible = false;
                    }
                    else
                    {
                        CusFirstpage.Visible = true;
                        CusPreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanelCustomer.Visible = false;
                    else
                        NavigationPanelCustomer.Visible = true;

                    #endregion
                }
            }
        }

        private void cusSearching(string _customer_code, string _customer_name)
        {
            DataTable dtSearCustomer = Customer.Instance.getCustomerByCondition(_customer_code.Trim(), _customer_name.Trim());
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("customer_code");
            dtSubReload.Columns.Add("customer_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            if (dtSearCustomer.Rows.Count > 0)
            {
                dgvCustomer.DataSource = dtSearCustomer;
                dgvCustomer.DataBind();

                //Calculate split page.
                int counter = 0;
                int pageIndex = Cus_currentPage - 1;
                int startIndex = Cus_pageSize * pageIndex;
                int endIndex = startIndex + Cus_pageSize - 1;

                if (dtSearCustomer.Rows.Count > 0 || dtSearCustomer != null)
                {
                    foreach (DataRow itemReload in dtSearCustomer.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["guid"].ToString().Trim();
                            drReload[1] = itemReload["customer_code"].ToString().Trim();
                            drReload[2] = itemReload["customer_name"].ToString().Trim();
                            drReload[3] = itemReload["is_active"].ToString().Trim();
                            drReload[4] = itemReload["create_date"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblCusSearchingCount.Visible = false;
                }
                else if (dtSearCustomer.Rows.Count == 0 || dtSearCustomer == null)
                {
                    //Not found data.
                    this.lblCusSearchingCount.Visible = true;
                    this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvCustomer.DataSource = dtSearCustomer;
                    dgvCustomer.DataBind();

                    CusNextButton.Visible = false;
                    CusEndpage.Visible = false;
                    CusPreviousButton.Visible = false;
                    CusFirstpage.Visible = false;

                    this.lblCuspageIndex.Visible = false;
                    this.txtCuspageIndex.Visible = false;
                    this.btnCusGO.Visible = false;

                    CusCurrentPageLabel.Text = "0";
                    CusTotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
                CusPreviousButton.Visible = true;
                CusFirstpage.Visible = true;

                dgvCustomer.DataSource = dtSubReload;

                Cus_totalPages = ((dtSearCustomer.Rows.Count - 1) / Cus_pageSize) + 1;

                if (Cus_currentPage > Cus_totalPages)
                {
                    Cus_currentPage = Cus_totalPages;
                    cusSearching(_customer_code, _customer_name);
                    return;
                }

                dgvCustomer.DataBind();
                CusCurrentPageLabel.Text = Cus_currentPage.ToString();
                CusTotalPagesLabel.Text = Cus_totalPages.ToString();

                if (this.CusTotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblCuspageIndex.Visible = false;
                    this.txtCuspageIndex.Visible = false;
                    this.btnCusGO.Visible = false;
                }
                else if (this.CusTotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblCuspageIndex.Visible = true;
                    this.txtCuspageIndex.Visible = true;
                    this.btnCusGO.Visible = true;
                }

                if (Cus_currentPage == Cus_totalPages)
                {
                    CusNextButton.Visible = false;
                    CusEndpage.Visible = false;
                }
                else
                {
                    CusNextButton.Visible = true;
                    CusEndpage.Visible = true;
                }

                if (Cus_currentPage == 1)
                {
                    CusFirstpage.Visible = false;
                    CusPreviousButton.Visible = false;
                }
                else
                {
                    CusFirstpage.Visible = true;
                    CusPreviousButton.Visible = true;
                }

                if (dtSearCustomer.Rows.Count <= 0)
                    NavigationPanelCustomer.Visible = false;
                else
                    NavigationPanelCustomer.Visible = true;
            }
            else
            {
                Page.ScriptJqueryMessage("Data not found", JMessageType.Warning);
                dtSearCustomer.Columns.Add("guid");
                dtSearCustomer.Columns.Add("customer_code");
                dtSearCustomer.Columns.Add("customer_name");
                dtSearCustomer.Columns.Add("is_active");
                dtSearCustomer.Columns.Add("create_date");
                this.dgvCustomer.DataSource = dtSearCustomer;
                this.dgvCustomer.DataBind();
            }
        }

        #endregion

        #region Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["secLogin"] == null)
                {
                    Response.Redirect("~/Login.aspx");
                }

                Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;

                // Check column in datatable.
                if (!this.dtRead.Columns.Contains("cust_id"))
                    this.dtRead.Columns.Add("cust_id");
                else if (!this.dtRead.Columns.Contains("cust_name"))
                    this.dtRead.Columns.Add("cust_name");
            }
            else
            {
                // Check column in datatable.
                if (!this.dtRead.Columns.Contains("cust_id"))
                    this.dtRead.Columns.Add("cust_id");
                else if (!this.dtRead.Columns.Contains("cust_name"))
                    this.dtRead.Columns.Add("cust_name");
            }
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            Tab3.CssClass = "Initial";
            MainView.ActiveViewIndex = 1;
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
        }
        protected void TabCustomer_Click(object sender, EventArgs e)
        {
            TabCustomer.CssClass = "Clicked";
            TabImportCustomer.CssClass = "Initial";
            TabItems.CssClass = "Initial";
            MultiViewMaster.ActiveViewIndex = 0;

            // Binding data customer.
            DataTable dtTabCustomer = Customer.Instance.getCustomer();
            if (dtTabCustomer.Rows.Count > 0)
            {
                Reload();
            }
            else
            {
                this.dgvCustomer.DataSource = dtTabCustomer;
                this.dgvCustomer.DataBind();
                NavigationPanelCustomer.Visible = false;
            }

            // Import Customer Controls.
            if (!this.dtRead.Columns.Contains("cust_id"))
                this.dtRead.Columns.Add("cust_id");
            else if (!this.dtRead.Columns.Contains("cust_name"))
                this.dtRead.Columns.Add("cust_name");

            // Clear customer control
            this.txtCustomerCode.Text = string.Empty;
            this.txtCustomerName.Text = string.Empty;

            PageSetFocus(txtCustomerCode);
        }
        protected void TabImportCustomer_Click(object sender, EventArgs e)
        {
            TabCustomer.CssClass = "Initial";
            TabItems.CssClass = "Initial";
            TabImportCustomer.CssClass = "Clicked";
            MultiViewMaster.ActiveViewIndex = 1;
        }
        protected void TabItems_Click(object sender, EventArgs e)
        {
            TabCustomer.CssClass = "Initial";
            TabImportCustomer.CssClass = "Initial";
            TabItems.CssClass = "Clicked";
            MultiViewMaster.ActiveViewIndex = 2;
            this.txtItemspageIndex.Text = string.Empty;
            // Binding data customer.
            if (DataAccess.Items.Items.Instance.getItems().Rows.Count > 0)
            {
                ReloadItems();
            }
            else
            {
                NavigationPanelItems.Visible = false;
                this.dgvItems.DataSource = DataAccess.Items.Items.Instance.getItems();
                this.dgvItems.DataBind();
            }

            this.txtTempItemCode.Text = string.Empty;
            this.txtFullItemCode.Text = string.Empty;
            this.txtItemName.Text = string.Empty;

            PageSetFocus(this.txtTempItemCode);
        }

        protected void btnReceiveData_Click(object sender, EventArgs e)
        {
            if (this.Tab1.CssClass.ToString() == "Clicked")
            {
                // Select Tab1.

            }
            else if(this.Tab2.CssClass.ToString() == "Clicked")
            {
                // Select Tab2.

            }
            else if(this.Tab3.CssClass.ToString() == "Clicked")
            {
                // Select Tab3.

            }
        }
        protected void btnAddMaster_Click(object sender, EventArgs e)
        {
            light.Style.Add("display", "block");
            fade.Style.Add("display", "block");

            TabCustomer.CssClass = "Clicked";
            TabImportCustomer.CssClass = "Initial";
            TabItems.CssClass = "Initial";
            MultiViewMaster.ActiveViewIndex = 0;

            // Control Customer.
            this.txtCustomerCode.Text = string.Empty;
            this.txtCustomerCode.Enabled = true;
            this.txtCustomerName.Text = string.Empty;
            this.chkIsActive.Checked = false;
            this.txtCuspageIndex.Text = string.Empty;
            // Binding data customer.
            DataTable dtAddCustomer = Customer.Instance.getCustomer();
            if (dtAddCustomer.Rows.Count > 0)
            {
                Reload();
            }
            else
            {
                this.dgvCustomer.DataSource = dtAddCustomer;
                this.dgvCustomer.DataBind();
                NavigationPanelCustomer.Visible = false;
            }

            // Import Customer Controls.
            if (!this.dtRead.Columns.Contains("cust_id"))
                this.dtRead.Columns.Add("cust_id");
            else if (!this.dtRead.Columns.Contains("cust_name"))
                this.dtRead.Columns.Add("cust_name");

            // Control Items.
            this.txtTempItemCode.Text = string.Empty;
            this.txtFullItemCode.Text = string.Empty;
            this.txtItemName.Text = string.Empty;

            PageSetFocus(txtCustomerCode);
        }
        protected void btnSaveCustomer_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtCustomerCode.Text.Trim()))
            {
                Page.ScriptJqueryMessage("Please enter customer code", JMessageType.Warning);
                PageSetFocus(txtCustomerCode);
                return;
            }
            else if (string.IsNullOrEmpty(this.txtCustomerName.Text.Trim()))
            {
                Page.ScriptJqueryMessage("Please enter customer name", JMessageType.Warning);
                PageSetFocus(txtCustomerName);
                return;
            }
            else
            {
                if (this.txtAction.Enabled)
                {
                    #region INSERT

                    if(Customer.Instance.SaveCustomer(Guid.NewGuid().ToString().ToUpper().Trim(), this.txtCustomerCode.Text.Trim(),
                            this.txtCustomerName.Text.Trim(), this.chkIsActive.Checked == true ? "YES" : "NO") == "0")
                    {
                        this.txtAction.Text = string.Empty;
                        this.txtCustomerCode.Enabled = true;
                        this.txtAction.Enabled = true;

                        // Clear control customer.
                        this.txtCustomerCode.Text = string.Empty;
                        this.txtCustomerName.Text = string.Empty;
                        this.chkIsActive.Checked = false;
                        Page.ScriptJqueryMessage("Save successfully", JMessageType.Accept);
                        Reload();
                    }
                    else
                    {
                        Page.ScriptJqueryMessage(Customer.Instance.SaveCustomer(Guid.NewGuid().ToString().ToUpper().Trim(), this.txtCustomerCode.Text.Trim(),
                            this.txtCustomerName.Text.Trim(), this.chkIsActive.Checked == true ? "YES" : "NO"), JMessageType.Warning);
                        return;
                    }

                    #endregion
                }
                else if (!this.txtAction.Enabled)
                {
                    #region EDIT

                    if (Customer.Instance.EditCustomer(this.txtAction.Text.Trim(), this.txtCustomerCode.Text.Trim(),
                            this.txtCustomerName.Text.Trim(), this.chkIsActive.Checked == true ? "YES" : "NO") == "0")
                    {
                        this.txtAction.Text = string.Empty;
                        this.txtCustomerCode.Enabled = true;
                        this.txtAction.Enabled = true;

                        // Clear control customer.
                        this.txtCustomerCode.Text = string.Empty;
                        this.txtCustomerName.Text = string.Empty;
                        this.chkIsActive.Checked = false;
                        Page.ScriptJqueryMessage("Edit successfully", JMessageType.Accept);
                        Reload();
                    }
                    else
                    {
                        Page.ScriptJqueryMessage(Customer.Instance.EditCustomer(this.txtAction.Text.Trim(), this.txtCustomerCode.Text.Trim(),
                            this.txtCustomerName.Text.Trim(), this.chkIsActive.Checked == true ? "YES" : "NO"), JMessageType.Warning);
                        return;
                    }

                    #endregion
                }
            }
        }
        protected void btnClearCustomer_Click(object sender, EventArgs e)
        {
            this.txtCustomerCode.Text = string.Empty;
            this.txtCustomerCode.Enabled = true;
            this.txtCustomerName.Text = string.Empty;
            this.chkIsActive.Checked = false;

            this.txtAction.Text = string.Empty;
            this.txtAction.Enabled = true;

            PageSetFocus(this.txtCustomerCode);
        }
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (upFile.HasFile)
            {
                #region Have File

                try
                {
                    #region Load file

                    string fileName = this.upFile.PostedFile.FileName;
                    //string TempfileLocation = Server.MapPath(this.upFile.PostedFile.FileName); // @"C:\UploadFiles\";
                    string getFullPath = Server.MapPath(this.upFile.PostedFile.FileName); // @"C:\UploadFiles\";
                    //string getFullPath = System.IO.Path.Combine(TempfileLocation, fileName);

                    this.upFile.SaveAs(getFullPath);
                    if (!string.IsNullOrEmpty(getFullPath))
                    {
                        if (Customer.Instance.getImportCustomer())
                        {
                            #region Have Customer

                            // Check column in datatable.
                            if (!this.dtRead.Columns.Contains("cust_id"))
                                this.dtRead.Columns.Add("cust_id");
                            else if (!this.dtRead.Columns.Contains("cust_name"))
                                this.dtRead.Columns.Add("cust_name");

                            using (StreamReader streamReader = new StreamReader(getFullPath, Encoding.GetEncoding("windows-874")))
                            {
                                string text = streamReader.ReadToEnd().Replace("\n", string.Empty);
                                string[] array = text.Split(new char[]
					                                {
						                                Convert.ToChar("\r")
					                                });

                                for (int i = 0; i < Enumerable.Count<string>(array); i++)
                                {
                                    if (i >= 0)
                                    {
                                        if (!string.IsNullOrEmpty(array[i]))
                                        {
                                            string[] array2 = array[i].Split(new char[]
								{
									Convert.ToChar('\t')
								});

                                            if (array2.Length == 3)
                                            {
                                                Page.ScriptJqueryMessage("Select file not type for customer", JMessageType.Warning);
                                                return;
                                            }

                                            this.dtRead.Rows.Add(new object[0]);
                                            int num = this.dtRead.Rows.Count - 1;

                                            // Insert data to datatable.
                                            this.dtRead.Rows[num]["cust_id"] = array2[0];
                                            this.dtRead.Rows[num]["cust_name"] = array2[1];
                                        }
                                    }
                                }
                                streamReader.Dispose();
                            }

                            if (this.dtRead.Rows.Count > 0)
                            {
                                if (!Customer.Instance.DeleteCustomerAll())
                                {
                                    Page.ScriptJqueryMessage("Can't import customer", JMessageType.Warning);
                                    return;
                                }
                                else
                                {
                                    List<entCustomer> _list = new List<entCustomer>();
                                    for (int i = 0; i < this.dtRead.Rows.Count; i++)
                                    {
                                        _list.Add(new entCustomer
                                        {
                                            // Insert data to list.
                                            customer_code = this.dtRead.Rows[i]["cust_id"].ToString().Trim(),
                                            customer_name = this.dtRead.Rows[i]["cust_name"].ToString().Trim(),
                                            is_active = "YES"
                                        });
                                    }

                                    // Save
                                    if (Customer.Instance.SaveImportCustomer(_list) == "0")
                                    {
                                        this.loading.Style.Add("display", "none");
                                        this.dtRead = new DataTable();
                                        this.dtRead.Columns.Add("cust_id");
                                        this.dtRead.Columns.Add("cust_name");

                                        // Display message
                                        Page.ScriptJqueryMessage("Import Successfully", JMessageType.Accept);
                                    }
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            #region Not have customer

                            // Check column in datatable.
                            if (!this.dtRead.Columns.Contains("cust_id"))
                                this.dtRead.Columns.Add("cust_id");
                            else if (!this.dtRead.Columns.Contains("cust_name"))
                                this.dtRead.Columns.Add("cust_name");

                            using (StreamReader streamReader = new StreamReader(getFullPath, Encoding.GetEncoding("windows-874")))
                            {
                                string text = streamReader.ReadToEnd().Replace("\n", string.Empty);
                                string[] array = text.Split(new char[]
					                                {
						                                Convert.ToChar("\r")
					                                });

                                for (int i = 0; i < Enumerable.Count<string>(array); i++)
                                {
                                    if (i >= 0)
                                    {
                                        if (!string.IsNullOrEmpty(array[i]))
                                        {
                                            string[] array2 = array[i].Split(new char[]
								{
									Convert.ToChar('\t')
								});

                                            if (array2.Length == 3)
                                            {
                                                Page.ScriptJqueryMessage("Select file not type for customer", JMessageType.Warning);
                                                return;
                                            }

                                            this.dtRead.Rows.Add(new object[0]);
                                            int num = this.dtRead.Rows.Count - 1;

                                            // Insert data to datatable.
                                            this.dtRead.Rows[num]["cust_id"] = array2[0];
                                            this.dtRead.Rows[num]["cust_name"] = array2[1];
                                        }
                                    }
                                }
                                streamReader.Dispose();
                            }

                            if (this.dtRead.Rows.Count > 0)
                            {
                                List<entCustomer> _list = new List<entCustomer>();
                                for (int i = 0; i < this.dtRead.Rows.Count; i++)
                                {
                                    _list.Add(new entCustomer
                                    {
                                        // Insert data to list.
                                        customer_code = this.dtRead.Rows[i]["cust_id"].ToString().Trim(),
                                        customer_name = this.dtRead.Rows[i]["cust_name"].ToString().Trim(),
                                        is_active = "YES"
                                    });
                                }

                                // Save
                                if (Customer.Instance.SaveImportCustomer(_list) == "0")
                                {
                                    this.loading.Style.Add("display", "none");
                                    this.dtRead = new DataTable();
                                    this.dtRead.Columns.Add("cust_id");
                                    this.dtRead.Columns.Add("cust_name");

                                    // Display message
                                    Page.ScriptJqueryMessage("Import Successfully", JMessageType.Accept);
                                }
                            }

                            #endregion
                        }
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    // Display messager error.
                    Page.ScriptJqueryMessage(ex.Message.ToString(), JMessageType.Error);
                }

                #endregion
            }
            else
            {
                #region Not file

                this.loading.Style.Add("display", "none");
                this.dtRead = new DataTable();
                this.dtRead.Columns.Add("cust_id");
                this.dtRead.Columns.Add("cust_name");

                #endregion
            }
        }

        protected void CusFirstpage_Click(object sender, EventArgs e)
        {
            try
            {
                startPaging(1);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("First page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void CusPreviousButton_Click(object sender, EventArgs e)
        {
            Cus_currentPage = Convert.ToInt32(CusCurrentPageLabel.Text);
            Cus_currentPage--;
            Reload();
        }
        protected void CusNextButton_Click(object sender, EventArgs e)
        {
            Cus_currentPage = Convert.ToInt32(CusCurrentPageLabel.Text);
            Cus_currentPage++;
            Reload();
        }
        protected void CusEndpage_Click(object sender, EventArgs e)
        {
            try
            {
                int lastPage = Convert.ToInt32(this.CusTotalPagesLabel.Text.Trim());
                if (lastPage > 0)
                {
                    endPaging(lastPage);
                }
                else if (lastPage <= 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("End page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void btnCusGO_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(this.txtCuspageIndex.Text.Trim()))
                    gotoPage(Convert.ToInt32(this.txtCuspageIndex.Text.Trim()));
                else
                    PageSetFocus(this.txtCuspageIndex);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Go to page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void ItemsFirstpage_Click(object sender, EventArgs e)
        {
            try
            {
                startPagingItems(1);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Item first page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void ItemsPreviousButton_Click(object sender, EventArgs e)
        {
            Items_currentPage = Convert.ToInt32(ItemsCurrentPageLabel.Text);
            Items_currentPage--;
            ReloadItems();
        }
        protected void ItemsNextButton_Click(object sender, EventArgs e)
        {
            Items_currentPage = Convert.ToInt32(ItemsCurrentPageLabel.Text);
            Items_currentPage++;
            ReloadItems();
        }
        protected void ItemsEndpage_Click(object sender, EventArgs e)
        {
            try
            {
                int lastPage = Convert.ToInt32(this.ItemsTotalPagesLabel.Text.Trim());
                if (lastPage > 0)
                    endPagingItems(lastPage);
                else if (lastPage <= 0)
                    return;
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Items End page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void btnItemsGO_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(this.txtItemspageIndex.Text.Trim()))
                    gotoPageItems(Convert.ToInt32(this.txtItemspageIndex.Text.Trim()));
                else
                    PageSetFocus(this.txtItemspageIndex);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Items Go to page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }

        protected void btnClearPathFile_Click(object sender, EventArgs e)
        {
            this.upFile.Dispose();
        }
        protected void btnSaveItem_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(this.txtTempItemCode.Text.Trim()))
            {
                Page.ScriptJqueryMessage("Please input temp item code", JMessageType.Warning);
                PageSetFocus(this.txtTempItemCode);
                return;
            }
            else if(string.IsNullOrEmpty(this.txtFullItemCode.Text.Trim()))
            {
                Page.ScriptJqueryMessage("Please input full item code", JMessageType.Warning);
                PageSetFocus(this.txtFullItemCode);
                return;
            }
            else if(string.IsNullOrEmpty(this.txtItemName.Text.Trim()))
            {
                Page.ScriptJqueryMessage("Please input item name", JMessageType.Warning);
                PageSetFocus(this.txtItemName);
                return;
            }
            else
            {
                if(this.txtItemAction.Enabled)
                {
                    #region INSERT

                    if(DataAccess.Items.Items.Instance.SaveItems(Guid.NewGuid().ToString().ToUpper().Trim(), this.txtTempItemCode.Text.Trim(), 
                        this.txtFullItemCode.Text.Trim(), this.txtItemName.Text.Trim(), this.chkItemIsActive.Checked == true ? "YES" : "NO") == "0")
                    {
                        this.txtTempItemCode.Text = string.Empty;
                        this.txtFullItemCode.Text = string.Empty;
                        this.txtItemName.Text = string.Empty;
                        this.chkItemIsActive.Checked = false;
                        Page.ScriptJqueryMessage("Save successfully", JMessageType.Accept);
                        ReloadItems();
                    }
                    else
                    {
                        Page.ScriptJqueryMessage(DataAccess.Items.Items.Instance.SaveItems(Guid.NewGuid().ToString().ToUpper().Trim(), this.txtTempItemCode.Text.Trim(),
                        this.txtFullItemCode.Text.Trim(), this.txtItemName.Text.Trim(), this.chkItemIsActive.Checked == true ? "YES" : "NO"), JMessageType.Warning);
                        return;
                    }

                    #endregion
                }
                else if(!this.txtItemAction.Enabled)
                {
                    #region EDIT

                    if(DataAccess.Items.Items.Instance.EditItems(this.txtItemAction.Text.Trim(), this.txtTempItemCode.Text.Trim(), 
                        this.txtFullItemCode.Text.Trim(), this.txtItemName.Text.Trim(), this.chkItemIsActive.Checked == true ? "YES" : "NO") == "0")
                    {
                        this.txtTempItemCode.Text = string.Empty;
                        this.txtFullItemCode.Text = string.Empty;
                        this.txtItemName.Text = string.Empty;
                        this.chkItemIsActive.Checked = false;
                        Page.ScriptJqueryMessage("Edit successfully", JMessageType.Accept);
                        ReloadItems();
                    }
                    else
                    {
                        Page.ScriptJqueryMessage(DataAccess.Items.Items.Instance.EditItems(this.txtItemAction.Text.Trim(), this.txtTempItemCode.Text.Trim(),
                        this.txtFullItemCode.Text.Trim(), this.txtItemName.Text.Trim(), this.chkItemIsActive.Checked == true ? "YES" : "NO"), JMessageType.Warning);
                        return;
                    }

                    #endregion
                }
            }
        }
        protected void btnClearItem_Click(object sender, EventArgs e)
        {
            this.txtTempItemCode.Text = string.Empty;
            this.txtFullItemCode.Text = string.Empty;
            this.txtItemName.Text = string.Empty;
            this.chkItemIsActive.Checked = false;

            this.txtItemAction.Text = string.Empty;
            this.txtItemAction.Enabled = true;

            PageSetFocus(this.txtTempItemCode);
        }
        protected void imgcloseted_Click(object sender, ImageClickEventArgs e)
        {
            light.Style.Add("display", "none");
            fade.Style.Add("display", "none");
        }

        // Button Export
        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (this.Tab1.CssClass.ToString() == "Clicked")
            {
                // Select Tab1.

            }
            else if (this.Tab2.CssClass.ToString() == "Clicked")
            {
                // Select Tab2.

            }
            else if (this.Tab3.CssClass.ToString() == "Clicked")
            {
                // Select Tab3.

            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void DeleteImageButton_Click(object sender, EventArgs e)
        {
            string value_DELETE = (sender as ImageButton).CommandArgument;
            if (!string.IsNullOrEmpty(value_DELETE))
            {
                if(Customer.Instance.DeleteCustomer(value_DELETE) == "0")
                {
                    // Clear control customer.
                    this.txtAction.Text = string.Empty;
                    this.txtCustomerCode.Enabled = true;
                    this.txtAction.Enabled = true;
                    this.txtCustomerCode.Text = string.Empty;
                    this.txtCustomerName.Text = string.Empty;
                    this.chkIsActive.Checked = false;

                    // Binding customer.
                    Reload();
                }
            }
            else
            {
                Page.ScriptJqueryMessage("Not found guid for delete customer", JMessageType.Warning);
                return;
            }
        }
        protected void EditImageButton_Click(object sender, EventArgs e)
        {
            string value_EDIT = (sender as ImageButton).CommandArgument;

            if (!string.IsNullOrEmpty(value_EDIT))
            {
                // EDIT
                DataTable dtCustomer = Customer.Instance.getCustomerDetail(value_EDIT);
                if (dtCustomer.Rows.Count > 0)
                {
                    this.txtCustomerCode.Text = dtCustomer.Rows[0]["customer_code"].ToString().Trim();
                    this.txtCustomerCode.Enabled = false;
                    this.txtAction.Text = dtCustomer.Rows[0]["guid"].ToString().Trim();
                    this.txtAction.Enabled = false;
                    this.txtCustomerName.Text = dtCustomer.Rows[0]["customer_name"].ToString().Trim();
                    this.chkIsActive.Checked = dtCustomer.Rows[0]["is_active"].ToString().Trim() == "NO" ? false : true;
                }
                else
                {
                    Page.ScriptJqueryMessage("Not found data", JMessageType.Warning);
                    return;
                }
            }
            else
            {
                Page.ScriptJqueryMessage("Not found guid for edit customer", JMessageType.Warning);
                return;
            }
        }
        protected void DeleteItemsImageButton_Click(object sender, EventArgs e)
        {
            string value_DELETE = (sender as ImageButton).CommandArgument;
            if (!string.IsNullOrEmpty(value_DELETE))
            {
                if (DataAccess.Items.Items.Instance.DeleteItems(value_DELETE) == "0")
                {
                    this.txtItemAction.Text = string.Empty;
                    this.txtItemAction.Enabled = true;
                    this.txtTempItemCode.Text = string.Empty;
                    this.txtFullItemCode.Text = string.Empty;
                    this.txtItemName.Text = string.Empty;
                    this.chkItemIsActive.Checked = false;

                    // Load items.
                    if (DataAccess.Items.Items.Instance.getItems().Rows.Count >= Items_pageSize)
                    {
                        ReloadItems();
                    }
                    else
                    {
                        if (DataAccess.Items.Items.Instance.getItems().Rows.Count == 0)
                        {
                            this.NavigationPanelItems.Visible = false;
                        }
                        this.dgvItems.DataSource = DataAccess.Items.Items.Instance.getItems();
                        this.dgvItems.DataBind();
                    }
                }
            }
            else
            {
                Page.ScriptJqueryMessage("Not found guid for delete items", JMessageType.Warning);
                return;
            }
        }
        protected void EditItemsImageButton_Click(object sender, EventArgs e)
        {
            string value_EDIT = (sender as ImageButton).CommandArgument;

            if (!string.IsNullOrEmpty(value_EDIT))
            {
                // EDIT
                DataTable dtItems = DataAccess.Items.Items.Instance.getItemsDetail(value_EDIT);
                if (dtItems.Rows.Count > 0)
                {
                    this.txtItemAction.Text = dtItems.Rows[0]["guid"].ToString().Trim();
                    this.txtItemAction.Enabled = false;
                    this.txtTempItemCode.Text = dtItems.Rows[0]["temp_item_code"].ToString().Trim();
                    this.txtFullItemCode.Text = dtItems.Rows[0]["full_item_code"].ToString().Trim();
                    this.txtItemName.Text = dtItems.Rows[0]["item_name"].ToString().Trim();
                    this.chkItemIsActive.Checked = dtItems.Rows[0]["is_active"].ToString().Trim() == "NO" ? false : true;
                }
                else
                {
                    Page.ScriptJqueryMessage("Not found data", JMessageType.Warning);
                    return;
                }
            }
            else
            {
                Page.ScriptJqueryMessage("Not found guid for edit items", JMessageType.Warning);
                return;
            }
        }

        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtCustomerCode.Text.Trim()) & string.IsNullOrEmpty(this.txtCustomerName.Text.Trim()))
                Reload();
            else
                cusSearching(this.txtCustomerCode.Text.Trim(), this.txtCustomerName.Text.Trim());
        }

        #endregion
    }
}