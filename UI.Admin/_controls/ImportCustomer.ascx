﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImportCustomer.ascx.cs" Inherits="UI.Admin._controls.ImportCustomer" %>
<link rel="stylesheet" href="../_css/button.css" />

<style type="text/css">
    .auto-style4 {
        width: 208px;
    }

    .auto-style5 {
        width: 80px;
    }
</style>

<asp:Panel ID="ImportCustomerFormPanel" runat="server">
    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
        <tr>
            <td class="auto-style5">
                <asp:Label ID="lblPathFile" runat="server" Text="File Import"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:FileUpload ID="upFile" runat="server"></asp:FileUpload>
            </td>
            <td rowspan="2">
                <asp:Button ID="btnImport" runat="server" CssClass="button button-action" Text="Import" OnClick="btnImport_Click"  />&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnClearPathFile" runat="server" CssClass="button button-royal" Text="Clear" OnClick="btnClearPathFile_Click" />&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
