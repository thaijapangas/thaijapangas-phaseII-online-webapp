﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mamgements.aspx.cs" Inherits="UI.Admin.Mamgements" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Thai Japan Gas</title>
    <link rel="stylesheet" href="_css/popup.css" />
    <link rel="stylesheet" href="_css/btnbootstrap.css" />
    <link rel="stylesheet" href="_css/button.css" />
    <link rel="stylesheet" type="text/css" href="../_css/main.css" />
    <link rel="stylesheet" type="text/css" href="../_css/styles.css" />
    <link rel="stylesheet" href="_css/element-view.css" />
    <script src="../_js/script.js" type="text/javascript"></script>
    <script src="_js/jquery-latest.min.js" type="text/javascript"></script>
    <script src="_js/jquery1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="_js/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[id*=dgvCustomer] td").hover(function () {
                $("td", $(this).closest("tr")).addClass("hover_row");
            }
            ,
            function () {
                $("td", $(this).closest("tr")).removeClass("hover_row");
            });

            $("[id*=dgvItems] td").hover(function () {
                $("td", $(this).closest("tr")).addClass("hover_row");
            }
            ,
            function () {
                $("td", $(this).closest("tr")).removeClass("hover_row");
            });
        });

        function selectText() {
            document.getElementById("<%=txtpageIndex.ClientID %>").select();
            document.getElementById("<%=txtpageIndex.ClientID %>").focus();
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function DisplayLoading() {
            if ($('#upFile').val().length) {
                document.getElementById("loading").style.display = 'block';
            }
            else {
                document.getElementById("loading").style.display = 'none';
            }
        }
    </script>

    <style type="text/css">
        .Initial {
            display: block;
            padding: 4px 18px 4px 18px;
            float: left;
            background: url("../_images/InitialImage.png") no-repeat right top;
            /*background-color:#089bfc;*/
            color: Black;
            font-weight: bold;
        }

            .Initial:hover {
                color: White;
                background: url("../_images/SelectedButton.png") no-repeat right top;
                /*background-color:#006699;*/
            }

        .Clicked {
            float: left;
            display: block;
            background: url("../_images/SelectedButton.png") no-repeat right top;
            /*background-color:#006699;*/
            padding: 4px 18px 4px 18px;
            color: Black;
            font-weight: bold;
            color: White;
        }

        .auto-style1 {
            width: 128px;
        }

        .auto-style4 {
            width: 208px;
        }

        .auto-style5 {
            width: 80px;
        }

        #loading-image {
            border-width: 0px;
            position: fixed;
            padding: 50px;
            left: 40%;
            top: 30%;
        }
    </style>
</head>
<body style="font-family: 'Open Sans', sans-serif;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server" />
        <%--<asp:UpdatePanel ID="upnmain" runat="server">
            <ContentTemplate>--%>
        <div id="loading" runat="server" style="position: fixed; text-align: center; display: none; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <%--<span style="border-width: 0px; position: fixed; padding: 50px; background-color: #FFFFFF; font-size: 36px; left: 40%; top: 40%;">Loading ...</span>--%>
            <img id="loading-image" src="_images/loading.gif" />
        </div>
        <div id="light" runat="server" class="white_content">
            <asp:ImageButton ID="imgcloseted" runat="server" CssClass="closeted" ToolTip="Close" ImageUrl="~/_images/close-icon.png" OnClick="imgcloseted_Click" />
            <table id="tbMasterData" style="width: 100%;">
                <tr style="background-color: #006699;">
                    <td>
                        <asp:Label ID="lblMasterData" runat="server" Text="Master Data" ForeColor="White"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 0px;"></td>
                </tr>
                <tr>
                    <td>
                        <asp:Button Text="Customer" BorderStyle="None" ID="TabCustomer" CssClass="Initial" runat="server" OnClick="TabCustomer_Click" />
                        <asp:Button Text="Import Customer" BorderStyle="None" ID="TabImportCustomer" CssClass="Initial" runat="server" OnClick="TabImportCustomer_Click" />
                        <asp:Button Text="Items" BorderStyle="None" ID="TabItems" CssClass="Initial" runat="server" OnClick="TabItems_Click" />
                        <asp:MultiView ID="MultiViewMaster" runat="server">
                            <asp:View ID="ViewCustomer" runat="server">
                                <asp:UpdatePanel ID="unpCustomer" runat="server">
                                    <ContentTemplate>
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td class="auto-style1">
                                                    <asp:Label ID="lblCustomerCode" runat="server" Text="Customer Code"></asp:Label>
                                                </td>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtCustomerCode" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:ImageButton ID="imgSearch" runat="server" ImageUrl="~/_images/search.png" Height="22px" Width="22px" OnClick="imgSearch_Click" />
                                                    <asp:TextBox ID="txtAction" runat="server" Visible="false"></asp:TextBox>
                                                </td>
                                                <td rowspan="2">
                                                    <asp:Button ID="btnSaveCustomer" runat="server" CssClass="button button-action" Text="Save" OnClick="btnSaveCustomer_Click" />&nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnClearCustomer" runat="server" CssClass="button button-royal" Text="Clear" OnClick="btnClearCustomer_Click" />&nbsp;&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style1">
                                                    <asp:Label ID="lblCustomerName" runat="server" Text="Customer Name"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                    
                                                </td>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtCustomerName" runat="server" Width="400px"></asp:TextBox>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style1">
                                                    <asp:Label ID="lblIsActive" runat="server" Text="Is Active"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                    
                                                </td>
                                                <td class="auto-style4">
                                                    <asp:CheckBox ID="chkIsActive" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:GridView ID="dgvCustomer" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemStyle Width="35px" />
                                                                <HeaderStyle CssClass="headerRow" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="EditImageButton" CommandArgument='<%# Eval("guid") %>' runat="server"
                                                                        ImageUrl="~/_images/edit.gif" OnClick="EditImageButton_Click"></asp:ImageButton>
                                                                    <asp:ImageButton ID="DeleteImageButton" ImageUrl="~/_images/delete.gif" runat="server"
                                                                        Visible="true" OnClick="DeleteImageButton_Click" CommandArgument='<%# Eval("guid") %>'
                                                                        OnClientClick="return confirm('ยืนยันลบรายการที่เลือก?');" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="guid" HeaderText="guid" Visible="False">
                                                                <HeaderStyle ForeColor="White" />
                                                                <ItemStyle Width="0px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="customer_code" HeaderText="Customer Code">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="customer_name" HeaderText="Customer Name">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="300px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="is_active" HeaderText="Active">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="create_date" HeaderText="Create Date">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <br />
                                                    <asp:Panel ID="NavigationPanelCustomer" Visible="true" runat="server">
                                                        <table border="0" cellpadding="3" cellspacing="3">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblCusSearchingCount" runat="server" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100">Page
                                                            <asp:Label ID="CusCurrentPageLabel" runat="server" />
                                                                    of
                                                            <asp:Label ID="CusTotalPagesLabel" runat="server" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="CusFirstpage" Text="<< Start" runat="server" Style="text-decoration: none; color: #089bfc;" OnClick="CusFirstpage_Click" />
                                                                </td>
                                                                <td style="width: 60px">
                                                                    <asp:LinkButton ID="CusPreviousButton" Text="< Prev" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="CusPreviousButton_Click" />
                                                                </td>
                                                                <td style="width: 60px">
                                                                    <asp:LinkButton ID="CusNextButton" Text="Next >" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="CusNextButton_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="CusEndpage" Text="End >>" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="CusEndpage_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblCuspageIndex" runat="server" Text="Page"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCuspageIndex" runat="server" Width="35px" MaxLength="4" Height="17px" onkeypress="return isNumberKey(event);"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btnCusGO" runat="server" Text="Go" Width="40px" Style="text-align: center;" CssClass="btnOpen btnOpen-5" Height="22px" OnClick="btnCusGO_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:View>
                            <asp:View ID="ViewImportCustomer" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td class="auto-style5">
                                            <asp:Label ID="lblPathFile" runat="server" Text="File Import"></asp:Label>
                                        </td>
                                        <td class="auto-style4">
                                            <asp:FileUpload ID="upFile" runat="server"></asp:FileUpload>
                                        </td>
                                        <td rowspan="2">
                                            <asp:Button ID="btnImport" runat="server" CssClass="button button-action" Text="Import" OnClick="btnImport_Click" OnClientClick="DisplayLoading();" />&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="btnClearPathFile" runat="server" CssClass="button button-royal" Text="Clear" OnClick="btnClearPathFile_Click" />&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="ViewItems" runat="server">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td class="auto-style1">
                                                    <asp:Label ID="lblTempItemCode" runat="server" Text="Temp Item Code"></asp:Label>
                                                </td>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtTempItemCode" runat="server"></asp:TextBox>
                                                    <asp:TextBox ID="txtItemAction" runat="server" Visible="false"></asp:TextBox>
                                                </td>
                                                <td rowspan="3">
                                                    <asp:Button ID="btnSaveItem" runat="server" CssClass="button button-action" Text="Save" OnClick="btnSaveItem_Click" /><br />
                                                    <br />
                                                    <asp:Button ID="btnClearItem" runat="server" CssClass="button button-royal" Text="Clear" OnClick="btnClearItem_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style1">
                                                    <asp:Label ID="lblFullItemCode" runat="server" Text="Full Item Code"></asp:Label>
                                                </td>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtFullItemCode" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style1">
                                                    <asp:Label ID="lblItemName" runat="server" Text="Item Name"></asp:Label>
                                                </td>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style1">
                                                    <asp:Label ID="lblItemIsActive" runat="server" Text="Is Active"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                    
                                                </td>
                                                <td class="auto-style4">
                                                    <asp:CheckBox ID="chkItemIsActive" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:GridView ID="dgvItems" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemStyle Width="35px" />
                                                                <HeaderStyle CssClass="headerRow" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="EditItemsImageButton" CommandArgument='<%# Eval("guid") %>' runat="server"
                                                                        ImageUrl="~/_images/edit.gif" OnClick="EditItemsImageButton_Click"></asp:ImageButton>
                                                                    <asp:ImageButton ID="DeleteItemsImageButton" ImageUrl="~/_images/delete.gif" runat="server"
                                                                        Visible="true" OnClick="DeleteItemsImageButton_Click" CommandArgument='<%# Eval("guid") %>'
                                                                        OnClientClick="return confirm('ยืนยันลบรายการที่เลือก?');" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="guid" HeaderText="guid" Visible="False">
                                                                <HeaderStyle ForeColor="White" />
                                                                <ItemStyle Width="0px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="temp_item_code" HeaderText="Temp Item Code">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="full_item_code" HeaderText="Full Item Code">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="item_name" HeaderText="Item Name">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="is_active" HeaderText="Active">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="create_date" HeaderText="Create Date">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <br />
                                                    <asp:Panel ID="NavigationPanelItems" Visible="true" runat="server">
                                                        <table border="0" cellpadding="3" cellspacing="3">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblItemsSearchingCount" runat="server" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100">Page
                                                            <asp:Label ID="ItemsCurrentPageLabel" runat="server" />
                                                                    of
                                                            <asp:Label ID="ItemsTotalPagesLabel" runat="server" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="ItemsFirstpage" Text="<< Start" runat="server" Style="text-decoration: none; color: #089bfc;" OnClick="ItemsFirstpage_Click" />
                                                                </td>
                                                                <td style="width: 60px">
                                                                    <asp:LinkButton ID="ItemsPreviousButton" Text="< Prev" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="ItemsPreviousButton_Click" />
                                                                </td>
                                                                <td style="width: 60px">
                                                                    <asp:LinkButton ID="ItemsNextButton" Text="Next >" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="ItemsNextButton_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="ItemsEndpage" Text="End >>" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="ItemsEndpage_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblItemspageIndex" runat="server" Text="Page"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtItemspageIndex" runat="server" Width="35px" MaxLength="4" Height="17px" onkeypress="return isNumberKey(event);"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btnItemsGO" runat="server" Text="Go" Width="40px" Style="text-align: center;" CssClass="btnOpen btnOpen-5" Height="22px" OnClick="btnItemsGO_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:View>
                        </asp:MultiView>
                    </td>
                </tr>
            </table>
        </div>
        <div id="fade" runat="server" class="black_overlay"></div>
        <asp:UpdatePanel ID="upnmain" runat="server">
            <ContentTemplate>
                <div class="header">
                    <div class="fullpage">
                        <div id='cssmenu'>
                            <table>
                                <tr>
                                    <td style="height: 20px;"></td>
                                </tr>
                            </table>
                            <div class="login" id="bt_login">
                                <a href="Login.aspx" style="text-decoration: none; color: black;">
                                    <asp:Label ID="lblLogout" runat="server" Text="Log out" ForeColor="White" Font-Bold="true" Width="55px"></asp:Label></a>
                            </div>
                        </div>
                    </div>
                </div>
                <table style="width: 98%; margin-left: auto; margin-right: auto;" cellpadding="1"
                    cellspacing="1">
                    <tr>
                        <td style="height: 45px;">
                            <a href="Mamgements.aspx">
                                <asp:Image ID="imgMain" runat="server" ImageUrl="~/_images/LOGOTJG2.png" ImageAlign="Left" Width="220px" Height="80px" ToolTip="Thai Japan Gas Co.,Ltd" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 30px;"></td>
                    </tr>
                    <tr>
                        <td>
                            <%--button button-action -> สีเขียว--%>
                            <%--button button-highlight -> สีส้ม--%>
                            <%--button button-caution -> สีแดง--%>
                            <%--button button-royal -> สีม่วง--%>
                            <asp:Button ID="btnReceiveData" runat="server" CssClass="button button-primary" Text="Receive Data" OnClick="btnReceiveData_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnAddMaster" runat="server" CssClass="button button-highlight" Text="Add Master" OnClick="btnAddMaster_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 20px;"></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnperson" runat="server" GroupingText="Transaction List" Width="98%" Style="text-align: left;">
                                <asp:Button Text="1.1.2 GI Ref Delivery" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                                    OnClick="Tab1_Click" />
                                <asp:Button Text="1.2 Distribution Ret" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
                                    OnClick="Tab2_Click" />
                                <asp:Button Text="1.4.1 GRProduction" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                                    OnClick="Tab3_Click" />
                                <asp:MultiView ID="MainView" runat="server">
                                    <asp:View ID="View1" runat="server">
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="dgvDisplay" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundField DataField="date" HeaderText="Date">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="time" HeaderText="Time">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="40px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="full_item_code" HeaderText="Item Code">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="90px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField HeaderText="TMSB Factory Code">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="40px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField HeaderText="Cylinder Number">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="customer_code" HeaderText="Customer Code">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="50px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <asp:View ID="View2" runat="server">
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <h3>View 2
                                                    </h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <asp:View ID="View3" runat="server">
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <h3>View 3
                                                    </h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </asp:MultiView>
                                <br />
                                <asp:Panel ID="NavigationPanel" Visible="true" runat="server">
                                    <table border="0" cellpadding="3" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSearchingCount" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100">Page
                                            <asp:Label ID="CurrentPageLabel" runat="server" />
                                                of
                                            <asp:Label ID="TotalPagesLabel" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="Firstpage" Text="<< Start" runat="server" Style="text-decoration: none; color: #089bfc;" />
                                            </td>
                                            <td style="width: 60px">
                                                <asp:LinkButton ID="PreviousButton" Text="< Prev" Style="text-decoration: none; color: #089bfc;" runat="server" />
                                            </td>
                                            <td style="width: 60px">
                                                <asp:LinkButton ID="NextButton" Text="Next >" Style="text-decoration: none; color: #089bfc;" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="Endpage" Text="End >>" Style="text-decoration: none; color: #089bfc;" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblpageIndex" runat="server" Text="Page"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtpageIndex" runat="server" Width="35px" MaxLength="4" Height="17px" onkeypress="return isNumberKey(event);"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnGO" runat="server" Text="Go" Width="40px" Style="text-align: center;" CssClass="btnOpen btnOpen-5" Height="22px" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 15px;"></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:Button ID="btnExport" runat="server" CssClass="btn btn-success" Text="Export" Visible="true" OnClick="btnExport_Click" />&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-danger" Text="Cancel" OnClick="btnCancel_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 15px;"></td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAddMaster" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
