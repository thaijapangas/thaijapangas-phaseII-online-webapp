﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="UI.Users.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Thai Japan Gas</title>

    <link rel="stylesheet" href="_css/btnbootstrap.css" />
    <link rel="stylesheet" href="_css/button.css" />
    <link rel="stylesheet" type="text/css" href="../_css/main.css" />
    <link rel="stylesheet" type="text/css" href="../_css/styles.css" />
    <script src="../_js/script.js" type="text/javascript"></script>
    <script src="_js/jquery-latest.min.js" type="text/javascript"></script>
    <script src="_js/jquery1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="_js/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[id*=dgvDisplay] td").hover(function () {
                $("td", $(this).closest("tr")).addClass("hover_row");
            }
            ,
            function () {
                $("td", $(this).closest("tr")).removeClass("hover_row");
            });
        });

        function selectText() {
            document.getElementById("<%=txtpageIndex.ClientID %>").select();
            document.getElementById("<%=txtpageIndex.ClientID %>").focus();
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <style type="text/css">
        .Initial {
            display: block;
            padding: 4px 18px 4px 18px;
            float: left;
            background: url("../_images/InitialImage.png") no-repeat right top;
            color: Black;
            font-weight: bold;
        }

            .Initial:hover {
                color: White;
                background: url("../_images/SelectedButton.png") no-repeat right top;
            }

        .Clicked {
            float: left;
            display: block;
            background: url("../_images/SelectedButton.png") no-repeat right top;
            padding: 4px 18px 4px 18px;
            color: Black;
            font-weight: bold;
            color: White;
        }
    </style>
</head>
<body style="font-family: 'Open Sans', sans-serif;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server" />
        <asp:UpdatePanel ID="upnmain" runat="server">
            <ContentTemplate>

                <div class="header">
                    <div class="fullpage">
                        <div id='cssmenu'>
                            <table>
                                <tr>
                                    <td style="height: 20px;"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <table style="width: 98%; margin-left: auto; margin-right: auto;" cellpadding="1"
                    cellspacing="1">
                    <tr>
                        <td style="height: 45px;">
                            <a href="Default.aspx">
                                <asp:Image ID="imgMain" runat="server" ImageUrl="~/_images/LOGOTJG2.png" ImageAlign="Left" Width="220px" Height="80px" ToolTip="Thai Japan Gas Co.,Ltd" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 30px;"></td>
                    </tr>
                    <tr>
                        <td>
                            <%--button button-action -> สีเขียว--%>
                            <%--button button-highlight -> สีส้ม--%>
                            <%--button button-caution -> สีแดง--%>
                            <%--button button-royal -> สีม่วง--%>
                            <asp:Button ID="btnReceiveData" runat="server" CssClass="button button-primary" Text="Receive Data" OnClick="btnReceiveData_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 20px;"></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnperson" runat="server" GroupingText="Transaction List" Width="98%" Style="text-align: left;">
                                <asp:Button Text="1.1.2 GI Ref Delivery" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                                    OnClick="Tab1_Click" />
                                <asp:Button Text="1.2 Distribution Ret" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
                                    OnClick="Tab2_Click" />
                                <asp:Button Text="1.4.1 GRProduction" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                                    OnClick="Tab3_Click" />
                                <asp:MultiView ID="MainView" runat="server">
                                    <asp:View ID="View1" runat="server">
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="dgvDisplay" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundField DataField="date" HeaderText="Date">
                                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                        <ItemStyle Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="time" HeaderText="Time">
                                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                        <ItemStyle Width="40px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="full_item_code" HeaderText="Item Code">
                                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="TMSB Factory Code">
                                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                        <ItemStyle Width="40px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="Cylinder Number">
                                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="customer_code" HeaderText="Customer Code">
                                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                        <ItemStyle Width="50px" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <asp:View ID="View2" runat="server">
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <h3>View 2
                                                    </h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <asp:View ID="View3" runat="server">
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <h3>View 3
                                                    </h3>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </asp:MultiView>
                                <br />
                                <asp:Panel ID="NavigationPanel" Visible="true" runat="server">
                                    <table border="0" cellpadding="3" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSearchingCount" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100">Page
                                            <asp:Label ID="CurrentPageLabel" runat="server" />
                                                of
                                            <asp:Label ID="TotalPagesLabel" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="Firstpage" Text="<< Start" runat="server" Style="text-decoration: none; color: #089bfc;" />
                                            </td>
                                            <td style="width: 60px">
                                                <asp:LinkButton ID="PreviousButton" Text="< Prev" Style="text-decoration: none; color: #089bfc;" runat="server" />
                                            </td>
                                            <td style="width: 60px">
                                                <asp:LinkButton ID="NextButton" Text="Next >" Style="text-decoration: none; color: #089bfc;" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="Endpage" Text="End >>" Style="text-decoration: none; color: #089bfc;" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblpageIndex" runat="server" Text="Page"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtpageIndex" runat="server" Width="35px" MaxLength="4" Height="17px" onkeypress="return isNumberKey(event);"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnGO" runat="server" Text="Go" Width="40px" Style="text-align: center;" CssClass="btnOpen btnOpen-5" Height="22px" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 15px;"></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:Button ID="btnExport" runat="server" CssClass="btn btn-success" Text="Export" Visible="true" OnClick="btnExport_Click" />&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-danger" Text="Cancel" OnClick="btnCancel_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 15px;"></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
