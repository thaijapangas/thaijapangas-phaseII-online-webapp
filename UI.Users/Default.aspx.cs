﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Users
{
    public partial class Default : System.Web.UI.Page
    {
        #region Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;
            }
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            Tab3.CssClass = "Initial";
            MainView.ActiveViewIndex = 1;
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
        }
        

        protected void btnReceiveData_Click(object sender, EventArgs e)
        {
            if (this.Tab1.CssClass.ToString() == "Clicked")
            {
                // Select Tab1.

            }
            else if (this.Tab2.CssClass.ToString() == "Clicked")
            {
                // Select Tab2.

            }
            else if (this.Tab3.CssClass.ToString() == "Clicked")
            {
                // Select Tab3.

            }
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (this.Tab1.CssClass.ToString() == "Clicked")
            {
                // Select Tab1.

            }
            else if (this.Tab2.CssClass.ToString() == "Clicked")
            {
                // Select Tab2.

            }
            else if (this.Tab3.CssClass.ToString() == "Clicked")
            {
                // Select Tab3.

            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
        

        #endregion
    }
}