﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Diagnostics;

namespace Utility
{
    public static class ObjectConverter
    {
        public static DataTable ToDataTable<T>(IEnumerable<T> MyObject)
        {
            DataTable table = new DataTable();
            DataColumnCollection dataColumns = table.Columns;

            try
            {
                PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));

                for (int i = 0; i < props.Count; i++)
                {
                    PropertyDescriptor prop = props[i];
                    dataColumns.Add(new DataColumn(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType));
                }

                object[] values = new object[props.Count];

                foreach (T item in MyObject)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item) ?? DBNull.Value;
                    }
                    table.Rows.Add(values);
                }
            }
            catch
            {
                throw;
            }

            return table;
        }
    }
}