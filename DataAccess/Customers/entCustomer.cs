﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Customers
{
    public class entCustomer
    {
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string is_active { get; set; }
    }
}