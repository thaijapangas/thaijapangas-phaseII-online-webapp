﻿using dbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Runtime.Serialization;
using System.Data;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Entitys.Customers;
using Utility;

namespace DataAccess.Customers
{
    public class Customer : IDisposable
    {
        #region Instance

        private static Customer _Instance = new Customer();
        public static Customer Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Customer();
                }
                return _Instance;
            }
        }

        #endregion

        #region Tab Customer

        public DataTable getCustomer()
        {
            DataTable dtReturn = new DataTable();
            try
            {
                using(CommonEntities db = new CommonEntities())
                {
                    var get_customer = ObjectConverter.ToDataTable(from rows in db.tbm_customer
                                                                   //where rows.is_active == "YES"
                                                                   orderby rows.customer_code ascending
                                                                   select new entCustomer
                                                                   {
                                                                       #region Filds

                                                                       guid = rows.guid,
                                                                       customer_code = rows.customer_code,
                                                                       customer_name = rows.customer_name,
                                                                       is_active = rows.is_active,
                                                                       create_date = rows.create_date

                                                                       #endregion
                                                                   });

                    if (get_customer.Rows.Count > 0)
                        dtReturn = get_customer;
                    else
                        dtReturn = new DataTable();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return dtReturn;
        }
        public DataTable getCustomerByCondition(string _customer_code, string _customer_name)
        {
            DataTable dtReturn = new DataTable();
            try
            {
                if (string.IsNullOrEmpty(_customer_code) && string.IsNullOrEmpty(_customer_name))// 0:0
                {
                    #region False : False

                    using (CommonEntities db = new CommonEntities())
                    {
                        var get_customer = ObjectConverter.ToDataTable(from rows in db.tbm_customer
                                                                       orderby rows.customer_code ascending
                                                                       select new entCustomer
                                                                       {
                                                                           #region Filds

                                                                           guid = rows.guid,
                                                                           customer_code = rows.customer_code,
                                                                           customer_name = rows.customer_name,
                                                                           is_active = rows.is_active,
                                                                           create_date = rows.create_date

                                                                           #endregion
                                                                       });

                        if (get_customer.Rows.Count > 0)
                            dtReturn = get_customer;
                        else
                            dtReturn = new DataTable();
                    }

                    #endregion
                }
                else if(!string.IsNullOrEmpty(_customer_code) && string.IsNullOrEmpty(_customer_name)) // 1:0
                {
                    #region True : False

                    using (CommonEntities db = new CommonEntities())
                    {
                        var get_customer = ObjectConverter.ToDataTable(from rows in db.tbm_customer
                                                                       where rows.customer_code == _customer_code
                                                                       orderby rows.customer_code ascending
                                                                       select new entCustomer
                                                                       {
                                                                           #region Filds

                                                                           guid = rows.guid,
                                                                           customer_code = rows.customer_code,
                                                                           customer_name = rows.customer_name,
                                                                           is_active = rows.is_active,
                                                                           create_date = rows.create_date

                                                                           #endregion
                                                                       });

                        if (get_customer.Rows.Count > 0)
                            dtReturn = get_customer;
                        else
                            dtReturn = new DataTable();
                    }

                    #endregion
                }
                else if(string.IsNullOrEmpty(_customer_code) && !string.IsNullOrEmpty(_customer_name)) // 0:1
                {
                    #region False : True

                    using (CommonEntities db = new CommonEntities())
                    {
                        var get_customer = ObjectConverter.ToDataTable(from rows in db.tbm_customer
                                                                       where rows.customer_name.Contains(_customer_name)
                                                                       orderby rows.customer_code ascending
                                                                       select new entCustomer
                                                                       {
                                                                           #region Filds

                                                                           guid = rows.guid,
                                                                           customer_code = rows.customer_code,
                                                                           customer_name = rows.customer_name,
                                                                           is_active = rows.is_active,
                                                                           create_date = rows.create_date

                                                                           #endregion
                                                                       });

                        if (get_customer.Rows.Count > 0)
                            dtReturn = get_customer;
                        else
                            dtReturn = new DataTable();
                    }

                    #endregion
                }
                else if(!string.IsNullOrEmpty(_customer_code) && !string.IsNullOrEmpty(_customer_name)) // 1:1
                {
                    #region True : True

                    using (CommonEntities db = new CommonEntities())
                    {
                        var get_customer = ObjectConverter.ToDataTable(from rows in db.tbm_customer
                                                                       where rows.customer_code == _customer_code &&
                                                                       rows.customer_name.Contains(_customer_name)
                                                                       orderby rows.customer_code ascending
                                                                       select new entCustomer
                                                                       {
                                                                           #region Filds

                                                                           guid = rows.guid,
                                                                           customer_code = rows.customer_code,
                                                                           customer_name = rows.customer_name,
                                                                           is_active = rows.is_active,
                                                                           create_date = rows.create_date

                                                                           #endregion
                                                                       });

                        if (get_customer.Rows.Count > 0)
                            dtReturn = get_customer;
                        else
                            dtReturn = new DataTable();
                    }

                    #endregion
                }
            }
            catch (Exception)
            {
                throw;
            }

            return dtReturn;
        }
        public DataTable getCustomerDetail(string _guid)
        {
            DataTable dtReturn = new DataTable();
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    var get_customer = ObjectConverter.ToDataTable(from rows in db.tbm_customer
                                                                   where rows.guid == _guid
                                                                   select new entCustomer
                                                                   {
                                                                       #region Filds

                                                                       guid = rows.guid,
                                                                       customer_code = rows.customer_code,
                                                                       customer_name = rows.customer_name,
                                                                       is_active = rows.is_active,
                                                                       create_date = rows.create_date

                                                                       #endregion
                                                                   });

                    if (get_customer.Rows.Count > 0)
                        dtReturn = get_customer;
                    else
                        dtReturn = new DataTable();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return dtReturn;
        }
        // Transaction
        public string SaveCustomer(string _guid, string _customer_code, string _customer_name, string _is_active)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    db.sp_tran_tbm_customer(_guid,
                                            "INSERT",
                                            string.IsNullOrEmpty(_customer_code) ? string.Empty : _customer_code,
                                            string.IsNullOrEmpty(_customer_name) ? string.Empty : _customer_name,
                                            string.IsNullOrEmpty(_is_active) ? string.Empty : _is_active,
                                            "ADMIN",
                                            _Errcode,
                                            _ErrMsg);

                    if (_Errcode.Value.ToString().Trim() == "0")
                        retuenResult = "0";
                    else if (_Errcode.Value.ToString().Trim() == "ER0098")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                    else if (_Errcode.Value.ToString().Trim() == "ER001")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                    else if (_Errcode.Value.ToString().Trim() == "ER002")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                    else if (_Errcode.Value.ToString().Trim() == "ER0099")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }
        public string EditCustomer(string _guid, string _customer_code, string _customer_name, string _is_active)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    var get_customer_detail = db.tbm_customer.Where(q => q.guid == _guid).FirstOrDefault();
                    if(get_customer_detail != null)
                    {
                        db.sp_tran_tbm_customer(_guid,
                                                "UPDATE",
                                                string.IsNullOrEmpty(_customer_code) ? string.Empty : _customer_code,
                                                string.IsNullOrEmpty(_customer_name) ? string.Empty : _customer_name,
                                                string.IsNullOrEmpty(_is_active) ? string.Empty : _is_active,
                                                "ADMIN",
                                                _Errcode,
                                                _ErrMsg);

                        if (_Errcode.Value.ToString().Trim() == "0")
                            retuenResult = "0";
                        else if (_Errcode.Value.ToString().Trim() == "ER0098")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER001")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER002")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER0099")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }
        public string DeleteCustomer(string _guid)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    var get_customer_detail = db.tbm_customer.Where(q => q.guid == _guid).FirstOrDefault();
                    if (get_customer_detail != null)
                    {
                        db.sp_tran_tbm_customer(_guid,
                                                "DELETE",
                                                string.IsNullOrEmpty(get_customer_detail.customer_code) ? string.Empty : get_customer_detail.customer_code,
                                                string.IsNullOrEmpty(get_customer_detail.customer_name) ? string.Empty : get_customer_detail.customer_name,
                                                string.IsNullOrEmpty(get_customer_detail.is_active) ? string.Empty : get_customer_detail.is_active,
                                                "ADMIN",
                                                _Errcode,
                                                _ErrMsg);

                        if (_Errcode.Value.ToString().Trim() == "0")
                            retuenResult = "0";
                        else if (_Errcode.Value.ToString().Trim() == "ER0098")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER001")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER002")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER0099")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }

        #endregion

        #region Tab Import Customer
        public bool getImportCustomer()
        {
            bool result = false;
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    var resultCustomer = from rows in db.tbm_customer
                                         select rows;

                    if (resultCustomer.Count() > 0)
                        result = true;
                    else
                        result = false;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public bool DeleteCustomerAll()
        {
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    db.Database.ExecuteSqlCommand("DELETE FROM tbm_customer");
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public string SaveImportCustomer(List<entCustomer> _list)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    foreach (var _ent in _list)
                    {
                        db.sp_tran_tbm_customer(Guid.NewGuid().ToString().ToUpper().Trim(),
                                                      "INSERT",
                                                      _ent.customer_code,
                                                      _ent.customer_name,
                                                      "YES",
                                                      "ADMIN",
                                                      _Errcode,
                                                      _ErrMsg);

                        if (_Errcode.Value.ToString().Trim() == "0")
                            retuenResult = "0";
                        else if (_Errcode.Value.ToString().Trim() == "ER0098")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER001")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER002")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER0099")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}