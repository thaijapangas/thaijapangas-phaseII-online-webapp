﻿using dbModels;
using Entitys.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace DataAccess.Items
{
    public class Items : IDisposable
    {
        #region Instance

        private static Items _Instance = new Items();
        public static Items Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Items();
                }
                return _Instance;
            }
        }

        #endregion

        #region Tab Items

        public DataTable getItems()
        {
            DataTable dtReturn = new DataTable();
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    var get_item = ObjectConverter.ToDataTable(from rows in db.tbm_item
                                                                   //where rows.is_active == "YES"
                                                                   orderby rows.create_date ascending
                                                                   select new entItems
                                                                   {
                                                                       #region Filds

                                                                       guid = rows.guid,
                                                                       temp_item_code = rows.temp_item_code,
                                                                       full_item_code = rows.full_item_code,
                                                                       item_name = rows.item_name,
                                                                       is_active = rows.is_active,
                                                                       create_date = rows.create_date.Value

                                                                       #endregion
                                                                   });

                    if (get_item.Rows.Count > 0)
                        dtReturn = get_item;
                    else
                        dtReturn = new DataTable();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return dtReturn;
        }
        public DataTable getItemsDetail(string _guid)
        {
            DataTable dtReturn = new DataTable();
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    var get_item = ObjectConverter.ToDataTable(from rows in db.tbm_item
                                                                   where rows.guid == _guid
                                                                   select new entItems
                                                                   {
                                                                       #region Filds

                                                                       guid = rows.guid,
                                                                       temp_item_code = rows.temp_item_code,
                                                                       full_item_code = rows.full_item_code,
                                                                       item_name = rows.item_name,
                                                                       is_active = rows.is_active,
                                                                       create_date = rows.create_date.Value

                                                                       #endregion
                                                                   });

                    if (get_item.Rows.Count > 0)
                        dtReturn = get_item;
                    else
                        dtReturn = new DataTable();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return dtReturn;
        }
        // Transaction
        public string SaveItems(string _guid, string _temp_item_code, string _full_item_code, string _item_name, string _is_active)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    db.sp_tran_tbm_item(_guid,
                                        "INSERT",
                                        string.IsNullOrEmpty(_temp_item_code) ? string.Empty : _temp_item_code,
                                        string.IsNullOrEmpty(_full_item_code) ? string.Empty : _full_item_code,
                                        string.IsNullOrEmpty(_item_name) ? string.Empty : _item_name,
                                        string.IsNullOrEmpty(_is_active) ? string.Empty : _is_active,
                                        "ADMIN",
                                        _Errcode,
                                        _ErrMsg);

                    if (_Errcode.Value.ToString().Trim() == "0")
                        retuenResult = "0";
                    else if (_Errcode.Value.ToString().Trim() == "ER0098")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                    else if (_Errcode.Value.ToString().Trim() == "ER001")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                    else if (_Errcode.Value.ToString().Trim() == "ER002")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                    else if (_Errcode.Value.ToString().Trim() == "ER0099")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }
        public string EditItems(string _guid, string _temp_item_code, string _full_item_code, string _item_name, string _is_active)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    var get_items_detail = db.tbm_item.Where(q => q.guid == _guid).FirstOrDefault();
                    if (get_items_detail != null)
                    {
                        db.sp_tran_tbm_item(_guid,
                                            "UPDATE",
                                            string.IsNullOrEmpty(_temp_item_code) ? string.Empty : _temp_item_code,
                                            string.IsNullOrEmpty(_full_item_code) ? string.Empty : _full_item_code,
                                            string.IsNullOrEmpty(_item_name) ? string.Empty : _item_name,
                                            string.IsNullOrEmpty(_is_active) ? string.Empty : _is_active,
                                            "ADMIN",
                                            _Errcode,
                                            _ErrMsg);

                        if (_Errcode.Value.ToString().Trim() == "0")
                            retuenResult = "0";
                        else if (_Errcode.Value.ToString().Trim() == "ER0098")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER001")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER002")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER0099")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }
        public string DeleteItems(string _guid)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                using (CommonEntities db = new CommonEntities())
                {
                    var get_items_detail = db.tbm_item.Where(q => q.guid == _guid).FirstOrDefault();
                    if (get_items_detail != null)
                    {
                        db.sp_tran_tbm_item(_guid,
                                            "DELETE",
                                            string.IsNullOrEmpty(get_items_detail.temp_item_code) ? string.Empty : get_items_detail.temp_item_code,
                                            string.IsNullOrEmpty(get_items_detail.full_item_code) ? string.Empty : get_items_detail.full_item_code,
                                            string.IsNullOrEmpty(get_items_detail.item_name) ? string.Empty : get_items_detail.item_name,
                                            string.IsNullOrEmpty(get_items_detail.is_active) ? string.Empty : get_items_detail.is_active,
                                            "ADMIN",
                                            _Errcode,
                                            _ErrMsg);

                        if (_Errcode.Value.ToString().Trim() == "0")
                            retuenResult = "0";
                        else if (_Errcode.Value.ToString().Trim() == "ER0098")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER001")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER002")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER0099")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}