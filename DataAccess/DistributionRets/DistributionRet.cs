﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DistributionRets
{
    public class DistributionRet : IDisposable
    {
        #region Instance

        private static DistributionRet _Instance = new DistributionRet();
        public static DistributionRet Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new DistributionRet();
                }
                return _Instance;
            }
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
